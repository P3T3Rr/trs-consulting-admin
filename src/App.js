import './App.css';
import Route from './route/route'
import Menu from "./components/menu/menu";
import { useEffect, useState } from 'react';
import MenuItems from './api/menu-items.json';
import { Login } from './components/login/login';

export default function App() {

  const init = () => {
    const UserLogged = localStorage.getItem("user");
    return UserLogged ? JSON.parse(UserLogged) : [];
  }

  const UserLogged = init();
  const [logged, setLogged] = useState(false);

  useEffect(() => {
    if (UserLogged.length !== 0) {
      setLogged(true);
    }
  }, [UserLogged])

  return (
    <div className="App">
      {logged ?
        <>
          <Menu Items={MenuItems} setLogged={setLogged}/>
          <Route />
        </>
        :
        <>
          <Login logged={logged} setLogged={setLogged} />
        </>

      }
    </div>
  );
}