import { faXmark } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useEffect } from 'react';
import './styles.css'

export default function Alert({ text, setOpen, style }) {
    useEffect(() => {
        setTimeout(close, 3500);
    }, [])

    function close (){
        setOpen(false)
    }

    return (
        <div className='notificationModal' style={style}>
            <button title='Cerrar' className='close-button' onClick={close}> 
            <FontAwesomeIcon icon={faXmark} 
            /></button>
            <p>{text}</p>
        </div>
    );
}