import { MobileMenu } from './mobileMenu'
import { DesktopMenu } from './desktopMenu'
import './styles.css'

export default function Menu({ Items, setLogged }) {

    return (
        <>
            <MobileMenu Items={Items} className={"main-mobile"} setLogged={setLogged}/>
            <DesktopMenu Items={Items} setLogged={setLogged}/>
        </>
    );
}