import { getAuth, GoogleAuthProvider, signInWithPopup } from "firebase/auth";
import { faCircleExclamation } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import icoGoogle from '../../assets/google.png';
import { useEffect, useState } from "react";
import { readAdmis } from "../../services";
import logo from '../../assets/Logo.png';
import './styles.css';

export const Login = ({ logged, setLogged }) => {

    const [users, setUsers] = useState([]);
    const [verified, setVerified] = useState(false);

    useEffect(() => {
        let data = readAdmis();
        data.then(function (result) {
            if (data != null) {
                setUsers(result);
            }
        });
        setVerified(false);
    }, [])


    const Logged = async () => {
        const provider = new GoogleAuthProvider();
        const auth = getAuth();

        await signInWithPopup(auth, provider)
            .then(async (result) => {
                setVerified(true);

                users.map((user) => {
                    if (user.column_2 === result.user.email) {
                        setLogged(true);
                        localStorage.setItem("user", JSON.stringify(result.user.email));
                    }
                })

            }).catch((error) => {
                console.log(error)
            });
    }

    return (
        <>
            <img src={logo} alt="Logo" className='logo' />
            <button onClick={Logged} className="login" >
                <img src={icoGoogle} alt="google" />
                Iniciar con Google
            </button>
            {verified && !logged ?
                <div className="verified"> <FontAwesomeIcon icon={faCircleExclamation} /> Usuario sin permisos</div>
                :
                null
            }
        </>
    )
}