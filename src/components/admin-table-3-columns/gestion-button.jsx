import './styles.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useState } from "react";
import Modal from '../modals/modal'

export default function Button({ element, button, SeeModal, SendModal ,UpdateModal, DeleteModal, Collection, setSended, setNotSended, setDeleted, setUpdated, setError }) {

    const [openModal, setOpenModal] = useState(false);
    const [bodyModal, setBodyModal] = useState()

    function OnClick() {
        if (button.title === "Abrir") {
            setBodyModal("See")
        }
        if (button.title === "Modificar") {
            setBodyModal("Edit")
        }
        if (button.title === "Eliminar") {
            setBodyModal("Delete")
        }
        if (button.title === "Enviar") {
            setBodyModal("Send")
        }
        setOpenModal(!openModal);
    }

    return (
        <>
            <button className="iconsLink" onClick={OnClick} title={button.title} style={button.style} >
                <FontAwesomeIcon icon={button.icon} />
            </button>

            {openModal && bodyModal === "See" ?
                <Modal open={openModal} setOpen={setOpenModal} Container={SeeModal} element={element} title={element.column_1}/>
            : openModal && bodyModal === "Edit" ?
                <Modal open={openModal} setOpen={setOpenModal} Container={UpdateModal} element={element} title={button.titleModal} Collection={Collection} setAlert={setUpdated} />
            : openModal && bodyModal === "Delete" ?
                <Modal open={openModal} setOpen={setOpenModal} Container={DeleteModal} element={element} title={button.titleModal} Collection={Collection} setAlert={setDeleted} setError={setError}/>
            : openModal && bodyModal === "Send" ?
                <Modal open={openModal} setOpen={setOpenModal} Container={SendModal} element={element} title={button.titleModal} Collection={Collection} setAlert={setSended} setError={setNotSended}/>
            : null
            }
        </>
    );
}