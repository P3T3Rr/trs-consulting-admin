import { collection, onSnapshot, query } from "firebase/firestore";
import { db } from "../../../../services/firebase-config";
import { readData } from '../../../../services/read';
import Cancel from '../../../cancel-button/button';
import { useEffect, useState } from 'react';
import Button from '../../../button/button';
import emailjs from 'emailjs-com';
import '../styles.css';

export default function Send({ element, setOpenModal, setError, setAlert }) {

    const [Promotions, setPromotions] = useState([]);
    const [Promotion, setPromotion] = useState();
    const [ErrorMessages, setErrors] = useState(false);

    useEffect(() => {
        update();
    }, [])

    const update = async () => {
        const snap = query(collection(db, 'promotions'));
        onSnapshot(snap, () => {
            let data = readData('promotions', setErrors, setErrors);
            data.then(function (result) {
                if (data != null) {
                    setPromotions(result);
                    setPromotion(result[0])
                }
            });
        });
    }

    const send = async () => {
        
            try {
                setOpenModal(false)
                
                var templateParams = {
                    'email': element.column_2,
                    'titulo': Promotion.column_1,
                    'descripcion': Promotion.column_2,
                };
                if (Promotion.images.length==1){
                    templateParams.imagen1=Promotion.images[0]
                }
                if (Promotion.images.length==2){
                    templateParams.imagen1=Promotion.images[0]
                    templateParams.imagen2=Promotion.images[1]
                }
                if (Promotion.images.length==3){
                    templateParams.imagen1=Promotion.images[0]
                    templateParams.imagen2=Promotion.images[1]
                    templateParams.imagen3=Promotion.images[2]
                }
                if (Promotion.images.length==4){
                    templateParams.imagen1=Promotion.images[0]
                    templateParams.imagen2=Promotion.images[1]
                    templateParams.imagen3=Promotion.images[2]
                    templateParams.imagen4=Promotion.images[3]
                }
                if (Promotion.images.length==5){
                    templateParams.imagen1=Promotion.images[0]
                    templateParams.imagen2=Promotion.images[1]
                    templateParams.imagen3=Promotion.images[2]
                    templateParams.imagen4=Promotion.images[3]
                    templateParams.imagen5=Promotion.images[4]
                }
                emailjs.send('gmail', 'template_wc0xlq8', templateParams, 'tRDgUdGtw4YKDlrAX')
                    .then(function (response) {
                        setAlert(true);
                    }, function (error) {
                        setError(true);
                    })
            }

            catch (error) {
                setError(true);
            }
        }
    

    const handleChange = (event) => {
        let promotion = JSON.parse(event.target.value)
        setPromotion(promotion);
    };

    return (
        <>
            <br />
            <form className='form_promotion'>
                <div><label>Promoción: </label> </div>
                <select className="drop_down" onChange={handleChange}>
                    {Promotions.map((promotion, key) =>
                        <option key={key} value={JSON.stringify(promotion)}>{promotion.column_1}</option>
                    )}
                </select>
            </form>
            <br />
            <br />
            <Cancel titulo={"Cancelar"} icon={""} OnClick={() => setOpenModal(false)} />
            <Button titulo={"Enviar"} icon={""} style={{ backgroundColor: "#3b97b7" }} OnClick={send} />
        </>
    );
}