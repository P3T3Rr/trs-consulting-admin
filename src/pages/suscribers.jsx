import Columns from "../api/JSON-suscribers/suscribers-table-columns.json"
import TableSuscribers from "../components/table-suscribers/table-suscribers";
import {faTrash,faPaperPlane } from '@fortawesome/free-solid-svg-icons';
import Delete from "../components/modals/delete/delete";
import Send from "../components/modals/suscribers/send/send"
import SendAll from "../components/modals/suscribers/send-all/send-all"

const Buttons = [
    {title:"Enviar", style:{ color: "rgb(59, 151, 183)", marginRight: "0" }, icon:faPaperPlane, titleModal: "Enviar promoción"}, 
    {title:"Eliminar", style:{ color: "red", marginRight: "0" }, icon:faTrash, titleModal: "Confirmación"}   
]

export default function Suscribers() {
    return (
        <TableSuscribers
            Columns={Columns}
            Collection={"suscribers"}
            Title={"Suscriptores"}
            titleModal={"Información completa"}
            messageButton={"Ok"} 
            Buttons={Buttons}
            SendAll={SendAll}        
            Delete={Delete} 
            Send={Send}           
        />
    );
}