"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.readInformation = exports.iterateData = exports.readAdmis = exports.readGalery = exports.readData = void 0;

var _firestore = require("firebase/firestore");

var _firebaseConfig = require("./firebase-config");

var _index = require("../adapters/index");

var readData = function readData(collectionName, setLoading, setUpdatePagination) {
  var data, information;
  return regeneratorRuntime.async(function readData$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          setUpdatePagination(true);
          _context.prev = 1;
          _context.next = 4;
          return regeneratorRuntime.awrap((0, _firestore.getDocs)((0, _firestore.query)((0, _firestore.collection)(_firebaseConfig.db, collectionName), (0, _firestore.orderBy)("name", "asc"))));

        case 4:
          data = _context.sent;
          information = iterateData(collectionName, data);
          setLoading(false);
          setUpdatePagination(false);
          return _context.abrupt("return", information);

        case 11:
          _context.prev = 11;
          _context.t0 = _context["catch"](1);
          console.log(_context.t0);

        case 14:
        case "end":
          return _context.stop();
      }
    }
  }, null, null, [[1, 11]]);
};

exports.readData = readData;

var readGalery = function readGalery(setLoading) {
  var data, information;
  return regeneratorRuntime.async(function readGalery$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.prev = 0;
          _context2.next = 3;
          return regeneratorRuntime.awrap((0, _firestore.getDocs)((0, _firestore.query)((0, _firestore.collection)(_firebaseConfig.db, "galery"), (0, _firestore.orderBy)("date", "desc"))));

        case 3:
          data = _context2.sent;
          information = iterateData("galery", data);
          setLoading(false);
          return _context2.abrupt("return", information);

        case 9:
          _context2.prev = 9;
          _context2.t0 = _context2["catch"](0);
          console.log(_context2.t0);

        case 12:
        case "end":
          return _context2.stop();
      }
    }
  }, null, null, [[0, 9]]);
};

exports.readGalery = readGalery;

var readAdmis = function readAdmis() {
  var data, information;
  return regeneratorRuntime.async(function readAdmis$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.prev = 0;
          _context3.next = 3;
          return regeneratorRuntime.awrap((0, _firestore.getDocs)((0, _firestore.query)((0, _firestore.collection)(_firebaseConfig.db, "administrators"))));

        case 3:
          data = _context3.sent;
          information = iterateData("administrators", data);
          return _context3.abrupt("return", information);

        case 8:
          _context3.prev = 8;
          _context3.t0 = _context3["catch"](0);
          console.log(_context3.t0);

        case 11:
        case "end":
          return _context3.stop();
      }
    }
  }, null, null, [[0, 8]]);
};

exports.readAdmis = readAdmis;

var iterateData = function iterateData(collectionName, data) {
  if (data.docs.length > 0) {
    // Run only if exists data
    var information = [];
    var number = 1;
    data.forEach(function (doc) {
      if (collectionName === "categories") {
        information.push((0, _index.createCategorieAdapter)(doc.id, doc.data(), number));
      }

      if (collectionName === "subcategories") {
        information.push((0, _index.createSubcategorieAdapter)(doc.id, doc.data(), number));
      }

      if (collectionName === "promotions") {
        information.push((0, _index.createPromotionAdapter)(doc.id, doc.data(), number));
      }

      if (collectionName === "administrators") {
        information.push((0, _index.createAdministratorAdapter)(doc.id, doc.data(), number));
      }

      if (collectionName === "galery") {
        information.push((0, _index.createGaleryAdapter)(doc.id, doc.data()));
      }

      if (collectionName === "general-information") {
        information.push((0, _index.createGeneralInformationAdapter)(doc.id, doc.data()));
      }

      if (collectionName === "suscribers") {
        information.push((0, _index.createSuscriberAdapter)(doc.id, doc.data(), number));
      }

      number += 1;
    });
    return information;
  } else {
    return null;
  }
};

exports.iterateData = iterateData;

var readInformation = function readInformation(setSpinner) {
  var data, information;
  return regeneratorRuntime.async(function readInformation$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.prev = 0;
          _context4.next = 3;
          return regeneratorRuntime.awrap((0, _firestore.getDocs)((0, _firestore.query)((0, _firestore.collection)(_firebaseConfig.db, "general-information"))));

        case 3:
          data = _context4.sent;
          information = iterateData("general-information", data);
          setSpinner(false);
          return _context4.abrupt("return", information);

        case 9:
          _context4.prev = 9;
          _context4.t0 = _context4["catch"](0);
          console.log(_context4.t0);

        case 12:
        case "end":
          return _context4.stop();
      }
    }
  }, null, null, [[0, 9]]);
};

exports.readInformation = readInformation;